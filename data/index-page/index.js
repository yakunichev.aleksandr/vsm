module.exports = Object.assign({}, {
	preview: require('./preview.json'),
	sale: require('./sale.json'),
	sales: require('./sales.json'),
	products: require('./products.json')
});
