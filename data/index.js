module.exports = Object.assign({}, {
	footer: require('./footer'),
	header: require('./header'),
	index: require('./index-page'),
	contacts: require('./contacts'),
	product: require('./product'),
	search: require('./search'),
	liked: require('./liked')
});
